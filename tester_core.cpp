#include <hls_stream.h>
#include <ap_axi_sdata.h>

typedef ap_axiu<32,2,5,6> AXI_VALUE;

struct asd
{
	int a;
	int b;
	int c;
	int d;
	int e;
	int f;
};
typedef struct asd asdf;

asdf Adder2(hls::stream<AXI_VALUE> &in_stream,
		hls::stream<AXI_VALUE> &last_stream,
		int searched
		/*hls::stream<AXI_VALUE> &out_stream*/);

int main()
{
	hls::stream<AXI_VALUE> input;
	hls::stream<AXI_VALUE> input2;
	//hls::stream<AXI_VALUE> output;

	for(int i=0; i<12; i++)
	{
		AXI_VALUE val;

		val.data  = i;
		val.keep  = 1;
		val.strb=1;
		val.user=1;
		val.id=0;
		val.dest=0;
		val.last = (i == 12-1);
		input << val;

		AXI_VALUE val2;

				val2.data  = i==11?1:i;
				val2.keep  = 1;
				val2.strb=1;
				val2.user=1;
				val2.id=0;
				val2.dest=0;
				val2.last = (i == 1024*768-1);
				input2 << val2;
	}

	asdf resu = Adder2(input, input2, 4);

	printf("%x %x %x %x %x %x", resu.a, resu.b, resu.c, resu.d, resu.e, resu.f);

	/*for(int i=0; i<32; i++)
		{
			AXI_VALUE val;

			output.read(val);
			printf("Value %d %d \n", i, (int)val.data);
		}*/

	return 0;
}
