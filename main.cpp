#include <hls_stream.h>
#include <ap_axi_sdata.h>

typedef ap_axiu<32,2,5,6> AXI_VALUE;

#define INPUT_SIZE (3840*2180)
#define OUTPUT_SIZE 32



struct asd
{
	int a;
	int b;
	int c;
	int d;
	int e;
	int f;
};

typedef struct asd asdf;

asdf Adder2(hls::stream<AXI_VALUE> &in_stream,
		hls::stream<AXI_VALUE> &last_stream,
		int searched
		/*hls::stream<AXI_VALUE> &out_stream*/)
{
#pragma HLS INTERFACE s_axilite port=searched bundle=CONTROL_BUS
#pragma HLS INTERFACE s_axilite port=return bundle=CONTROL_BUS
//#pragma HLS INTERFACE axis port=out_stream name=OUTPUT_STREAM
#pragma HLS INTERFACE axis port=in_stream name=INPUT_STREAM
#pragma HLS INTERFACE axis port=last_stream name=LAST_STREAM

	int last=0;
	int differentBytes = 0;
	int in1Count=0,in2Count=0;
	AXI_VALUE inputValues[1024*76];
	AXI_VALUE lastValues[1024];
	AXI_VALUE aValue, bValue;
	int i;
	ap_uint<64> sum = 0;

	int suma = 0, diff;
	int j,val1, val2;
	int first = 0xFFFFF, second = 0xFFFFF;

	read_A: for(i=0; i< INPUT_SIZE; i++){
#pragma HLS PIPELINE
		in_stream.read(aValue);

		last_stream.read(bValue);

		if(aValue.data != bValue.data)
		{
			differentBytes++;
			sum = 0;
			for(j = 0; j<4;j++)
			{
				val1 = (aValue.data >> (j * 8)) & 0xFF;
				val2 = (bValue.data >> (j * 8)) & 0xFF;

				diff = val2 - val1;
				if(0 > diff)
				{
					diff *= -1;
				}

				suma += diff;

			}
			sum += suma;
		}

		if(in2Count == 0)
		{
			first = aValue.data;
			second = bValue.data;
		}

		//suma += aValue.data;
		in1Count++;
		in2Count++;

		if(aValue.last == 1)
		{
			break;
		}
//		A[i] = aValue;

}


	asdf a;
	a.a = differentBytes;
	a.b = in1Count;
	a.c = in2Count;
	a.d = sum >> 32;
	a.e = sum & 0xFFFFFFFF;
	a.f = 0x790;
	return a;
	//return (input[0] << 32) | searched;


	/*write_res: for(i=0; i< OUTPUT_SIZE; i++){
	#pragma HLS PIPELINE
			aValue.data = input[i] + 1;
			//aValue.data = mat_res[i][j];
			aValue.last = inputValues[i].last;//(i==OUTPUT_SIZE-1)? 1 : 0;
			aValue.strb = inputValues[i].strb;
			aValue.keep = inputValues[i].keep; //e.strb;
			aValue.user = inputValues[i].user;
			aValue.id = inputValues[i].id;
			aValue.dest = inputValues[i].dest;
	//		aValue = OUT[i];
			out_stream.write(aValue);
		}
*/
	//return input[0];
}
