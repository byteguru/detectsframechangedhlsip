; ModuleID = 'D:/Beni/Vhdl/Hls/AdderIp/solution1/.autopilot/db/a.o.2.bc'
target datalayout = "e-p:64:64:64-i1:8:8-i8:8:8-i16:16:16-i32:32:32-i64:64:64-f32:32:32-f64:64:64-v64:64:64-v128:128:128-a0:0:64-s0:64:64-f80:128:128-n8:16:32:64-S128"
target triple = "x86_64-w64-mingw32"

@llvm_global_ctors_1 = appending global [0 x void ()*] zeroinitializer
@llvm_global_ctors_0 = appending global [0 x i32] zeroinitializer
@Adder2_str = internal unnamed_addr constant [7 x i8] c"Adder2\00"
@p_str7 = private unnamed_addr constant [7 x i8] c"read_A\00", align 1
@p_str6 = private unnamed_addr constant [12 x i8] c"LAST_STREAM\00", align 1
@p_str5 = private unnamed_addr constant [13 x i8] c"INPUT_STREAM\00", align 1
@p_str4 = private unnamed_addr constant [5 x i8] c"both\00", align 1
@p_str3 = private unnamed_addr constant [5 x i8] c"axis\00", align 1
@p_str2 = private unnamed_addr constant [12 x i8] c"CONTROL_BUS\00", align 1
@p_str1 = private unnamed_addr constant [1 x i8] zeroinitializer, align 1
@p_str = private unnamed_addr constant [10 x i8] c"s_axilite\00", align 1

declare i64 @llvm.part.select.i64(i64, i32, i32) nounwind readnone

declare i32 @llvm.part.select.i32(i32, i32, i32) nounwind readnone

declare void @llvm.dbg.value(metadata, i64, metadata) nounwind readnone

declare void @llvm.dbg.declare(metadata, metadata) nounwind readnone

define weak void @_ssdm_op_Write.s_axilite.i32P(i32*, i32) {
entry:
  store i32 %1, i32* %0
  ret void
}

define weak void @_ssdm_op_SpecTopModule(...) {
entry:
  ret void
}

define weak i32 @_ssdm_op_SpecRegionEnd(...) {
entry:
  ret i32 0
}

define weak i32 @_ssdm_op_SpecRegionBegin(...) {
entry:
  ret i32 0
}

define weak void @_ssdm_op_SpecPipeline(...) nounwind {
entry:
  ret void
}

define weak i32 @_ssdm_op_SpecLoopTripCount(...) {
entry:
  ret i32 0
}

define weak void @_ssdm_op_SpecLoopName(...) nounwind {
entry:
  ret void
}

define weak void @_ssdm_op_SpecInterface(...) nounwind {
entry:
  ret void
}

define weak void @_ssdm_op_SpecBitsMap(...) {
entry:
  ret void
}

define weak { i32, i4, i4, i2, i1, i5, i6 } @_ssdm_op_Read.axis.volatile.i32P.i4P.i4P.i2P.i1P.i5P.i6P(i32*, i4*, i4*, i2*, i1*, i5*, i6*) {
entry:
  %empty = load i32* %0
  %empty_7 = load i4* %1
  %empty_8 = load i4* %2
  %empty_9 = load i2* %3
  %empty_10 = load i1* %4
  %empty_11 = load i5* %5
  %empty_12 = load i6* %6
  %mrv_0 = insertvalue { i32, i4, i4, i2, i1, i5, i6 } undef, i32 %empty, 0
  %mrv1 = insertvalue { i32, i4, i4, i2, i1, i5, i6 } %mrv_0, i4 %empty_7, 1
  %mrv2 = insertvalue { i32, i4, i4, i2, i1, i5, i6 } %mrv1, i4 %empty_8, 2
  %mrv3 = insertvalue { i32, i4, i4, i2, i1, i5, i6 } %mrv2, i2 %empty_9, 3
  %mrv4 = insertvalue { i32, i4, i4, i2, i1, i5, i6 } %mrv3, i1 %empty_10, 4
  %mrv5 = insertvalue { i32, i4, i4, i2, i1, i5, i6 } %mrv4, i5 %empty_11, 5
  %mrv6 = insertvalue { i32, i4, i4, i2, i1, i5, i6 } %mrv5, i6 %empty_12, 6
  ret { i32, i4, i4, i2, i1, i5, i6 } %mrv6
}

define weak i8 @_ssdm_op_PartSelect.i8.i32.i32.i32(i32, i32, i32) nounwind readnone {
entry:
  %empty = call i32 @llvm.part.select.i32(i32 %0, i32 %1, i32 %2)
  %empty_13 = trunc i32 %empty to i8
  ret i8 %empty_13
}

define weak i32 @_ssdm_op_PartSelect.i32.i64.i32.i32(i64, i32, i32) nounwind readnone {
entry:
  %empty = call i64 @llvm.part.select.i64(i64 %0, i32 %1, i32 %2)
  %empty_14 = trunc i64 %empty to i32
  ret i32 %empty_14
}

define weak i1 @_ssdm_op_BitSelect.i1.i9.i32(i9, i32) nounwind readnone {
entry:
  %empty = trunc i32 %1 to i9
  %empty_15 = shl i9 1, %empty
  %empty_16 = and i9 %0, %empty_15
  %empty_17 = icmp ne i9 %empty_16, 0
  ret i1 %empty_17
}

define void @Adder2(i32* noalias %agg_result_a, i32* noalias %agg_result_b, i32* noalias %agg_result_c, i32* noalias %agg_result_d, i32* noalias %agg_result_e, i32* noalias %agg_result_f, i32* %INPUT_STREAM_V_data_V, i4* %INPUT_STREAM_V_keep_V, i4* %INPUT_STREAM_V_strb_V, i2* %INPUT_STREAM_V_user_V, i1* %INPUT_STREAM_V_last_V, i5* %INPUT_STREAM_V_id_V, i6* %INPUT_STREAM_V_dest_V, i32* %LAST_STREAM_V_data_V, i4* %LAST_STREAM_V_keep_V, i4* %LAST_STREAM_V_strb_V, i2* %LAST_STREAM_V_user_V, i1* %LAST_STREAM_V_last_V, i5* %LAST_STREAM_V_id_V, i6* %LAST_STREAM_V_dest_V, i32 %searched) {
arrayctor.loop1.preheader:
  call void (...)* @_ssdm_op_SpecBitsMap(i32* %agg_result_a), !map !77
  call void (...)* @_ssdm_op_SpecBitsMap(i32* %agg_result_b), !map !81
  call void (...)* @_ssdm_op_SpecBitsMap(i32* %agg_result_c), !map !85
  call void (...)* @_ssdm_op_SpecBitsMap(i32* %agg_result_d), !map !89
  call void (...)* @_ssdm_op_SpecBitsMap(i32* %agg_result_e), !map !93
  call void (...)* @_ssdm_op_SpecBitsMap(i32* %agg_result_f), !map !97
  call void (...)* @_ssdm_op_SpecBitsMap(i32* %INPUT_STREAM_V_data_V), !map !101
  call void (...)* @_ssdm_op_SpecBitsMap(i4* %INPUT_STREAM_V_keep_V), !map !105
  call void (...)* @_ssdm_op_SpecBitsMap(i4* %INPUT_STREAM_V_strb_V), !map !109
  call void (...)* @_ssdm_op_SpecBitsMap(i2* %INPUT_STREAM_V_user_V), !map !113
  call void (...)* @_ssdm_op_SpecBitsMap(i1* %INPUT_STREAM_V_last_V), !map !117
  call void (...)* @_ssdm_op_SpecBitsMap(i5* %INPUT_STREAM_V_id_V), !map !121
  call void (...)* @_ssdm_op_SpecBitsMap(i6* %INPUT_STREAM_V_dest_V), !map !125
  call void (...)* @_ssdm_op_SpecBitsMap(i32* %LAST_STREAM_V_data_V), !map !129
  call void (...)* @_ssdm_op_SpecBitsMap(i4* %LAST_STREAM_V_keep_V), !map !133
  call void (...)* @_ssdm_op_SpecBitsMap(i4* %LAST_STREAM_V_strb_V), !map !137
  call void (...)* @_ssdm_op_SpecBitsMap(i2* %LAST_STREAM_V_user_V), !map !141
  call void (...)* @_ssdm_op_SpecBitsMap(i1* %LAST_STREAM_V_last_V), !map !145
  call void (...)* @_ssdm_op_SpecBitsMap(i5* %LAST_STREAM_V_id_V), !map !149
  call void (...)* @_ssdm_op_SpecBitsMap(i6* %LAST_STREAM_V_dest_V), !map !153
  call void (...)* @_ssdm_op_SpecBitsMap(i32 %searched), !map !157
  call void (...)* @_ssdm_op_SpecTopModule([7 x i8]* @Adder2_str) nounwind
  call void (...)* @_ssdm_op_SpecInterface(i32 %searched, [10 x i8]* @p_str, i32 0, i32 0, [1 x i8]* @p_str1, i32 0, i32 0, [12 x i8]* @p_str2, [1 x i8]* @p_str1, [1 x i8]* @p_str1, i32 0, i32 0, i32 0, i32 0, [1 x i8]* @p_str1, [1 x i8]* @p_str1) nounwind
  call void (...)* @_ssdm_op_SpecInterface(i32 0, [10 x i8]* @p_str, i32 0, i32 0, [1 x i8]* @p_str1, i32 0, i32 0, [12 x i8]* @p_str2, [1 x i8]* @p_str1, [1 x i8]* @p_str1, i32 0, i32 0, i32 0, i32 0, [1 x i8]* @p_str1, [1 x i8]* @p_str1) nounwind
  call void (...)* @_ssdm_op_SpecInterface(i32* %agg_result_a, i32* %agg_result_b, i32* %agg_result_c, i32* %agg_result_d, i32* %agg_result_e, i32* %agg_result_f, [10 x i8]* @p_str, i32 0, i32 0, [1 x i8]* @p_str1, i32 0, i32 0, [12 x i8]* @p_str2, [1 x i8]* @p_str1, [1 x i8]* @p_str1, i32 0, i32 0, i32 0, i32 0, [1 x i8]* @p_str1, [1 x i8]* @p_str1) nounwind
  call void (...)* @_ssdm_op_SpecInterface(i32* %INPUT_STREAM_V_data_V, i4* %INPUT_STREAM_V_keep_V, i4* %INPUT_STREAM_V_strb_V, i2* %INPUT_STREAM_V_user_V, i1* %INPUT_STREAM_V_last_V, i5* %INPUT_STREAM_V_id_V, i6* %INPUT_STREAM_V_dest_V, [5 x i8]* @p_str3, i32 1, i32 1, [5 x i8]* @p_str4, i32 0, i32 0, [1 x i8]* @p_str1, [1 x i8]* @p_str1, [1 x i8]* @p_str1, i32 0, i32 0, i32 0, i32 0, [13 x i8]* @p_str5, [1 x i8]* @p_str1) nounwind
  call void (...)* @_ssdm_op_SpecInterface(i32* %LAST_STREAM_V_data_V, i4* %LAST_STREAM_V_keep_V, i4* %LAST_STREAM_V_strb_V, i2* %LAST_STREAM_V_user_V, i1* %LAST_STREAM_V_last_V, i5* %LAST_STREAM_V_id_V, i6* %LAST_STREAM_V_dest_V, [5 x i8]* @p_str3, i32 1, i32 1, [5 x i8]* @p_str4, i32 0, i32 0, [1 x i8]* @p_str1, [1 x i8]* @p_str1, [1 x i8]* @p_str1, i32 0, i32 0, i32 0, i32 0, [12 x i8]* @p_str6, [1 x i8]* @p_str1) nounwind
  br label %0

; <label>:0                                       ; preds = %1, %arrayctor.loop1.preheader
  %p_0 = phi i64 [ 0, %arrayctor.loop1.preheader ], [ %sum_V_1, %1 ]
  %differentBytes = phi i32 [ 0, %arrayctor.loop1.preheader ], [ %differentBytes_1, %1 ]
  %in1Count = phi i23 [ 0, %arrayctor.loop1.preheader ], [ %in1Count_3, %1 ]
  %suma = phi i32 [ 0, %arrayctor.loop1.preheader ], [ %suma_2, %1 ]
  %tmp = icmp ult i23 %in1Count, -17408
  %in1Count_3 = add i23 %in1Count, 1
  br i1 %tmp, label %_ifconv, label %.loopexit

_ifconv:                                          ; preds = %0
  %empty = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 1, i64 8371200, i64 4185600)
  call void (...)* @_ssdm_op_SpecLoopName([7 x i8]* @p_str7) nounwind
  %tmp_4 = call i32 (...)* @_ssdm_op_SpecRegionBegin([7 x i8]* @p_str7)
  call void (...)* @_ssdm_op_SpecPipeline(i32 -1, i32 1, i32 1, i32 0, [1 x i8]* @p_str1) nounwind
  %empty_18 = call { i32, i4, i4, i2, i1, i5, i6 } @_ssdm_op_Read.axis.volatile.i32P.i4P.i4P.i2P.i1P.i5P.i6P(i32* %INPUT_STREAM_V_data_V, i4* %INPUT_STREAM_V_keep_V, i4* %INPUT_STREAM_V_strb_V, i2* %INPUT_STREAM_V_user_V, i1* %INPUT_STREAM_V_last_V, i5* %INPUT_STREAM_V_id_V, i6* %INPUT_STREAM_V_dest_V)
  %tmp_data_V = extractvalue { i32, i4, i4, i2, i1, i5, i6 } %empty_18, 0
  %tmp_last_V = extractvalue { i32, i4, i4, i2, i1, i5, i6 } %empty_18, 4
  %tmp_2 = trunc i32 %tmp_data_V to i8
  %empty_19 = call { i32, i4, i4, i2, i1, i5, i6 } @_ssdm_op_Read.axis.volatile.i32P.i4P.i4P.i2P.i1P.i5P.i6P(i32* %LAST_STREAM_V_data_V, i4* %LAST_STREAM_V_keep_V, i4* %LAST_STREAM_V_strb_V, i2* %LAST_STREAM_V_user_V, i1* %LAST_STREAM_V_last_V, i5* %LAST_STREAM_V_id_V, i6* %LAST_STREAM_V_dest_V)
  %tmp_data_V_1 = extractvalue { i32, i4, i4, i2, i1, i5, i6 } %empty_19, 0
  %tmp_6 = trunc i32 %tmp_data_V_1 to i8
  %tmp_3 = icmp eq i32 %tmp_data_V, %tmp_data_V_1
  %differentBytes_3 = add nsw i32 1, %differentBytes
  %phitmp1_cast = zext i8 %tmp_2 to i9
  %phitmp2_cast = zext i8 %tmp_6 to i9
  %diff = sub i9 %phitmp2_cast, %phitmp1_cast
  %tmp_7 = call i1 @_ssdm_op_BitSelect.i1.i9.i32(i9 %diff, i32 8)
  %diff_1 = sub i9 0, %diff
  %diff_2 = select i1 %tmp_7, i9 %diff_1, i9 %diff
  %diff_2_cast = sext i9 %diff_2 to i10
  %tmp_8_1_cast = call i8 @_ssdm_op_PartSelect.i8.i32.i32.i32(i32 %tmp_data_V, i32 8, i32 15)
  %phitmp1_1_cast = zext i8 %tmp_8_1_cast to i9
  %tmp_4_1_cast = call i8 @_ssdm_op_PartSelect.i8.i32.i32.i32(i32 %tmp_data_V_1, i32 8, i32 15)
  %phitmp2_1_cast = zext i8 %tmp_4_1_cast to i9
  %diff_s = sub i9 %phitmp2_1_cast, %phitmp1_1_cast
  %tmp_8 = call i1 @_ssdm_op_BitSelect.i1.i9.i32(i9 %diff_s, i32 8)
  %diff_1_1 = sub i9 0, %diff_s
  %diff_2_1 = select i1 %tmp_8, i9 %diff_1_1, i9 %diff_s
  %diff_2_1_cast = sext i9 %diff_2_1 to i10
  %tmp_8_2_cast = call i8 @_ssdm_op_PartSelect.i8.i32.i32.i32(i32 %tmp_data_V, i32 16, i32 23)
  %phitmp1_2_cast = zext i8 %tmp_8_2_cast to i9
  %tmp_4_2_cast = call i8 @_ssdm_op_PartSelect.i8.i32.i32.i32(i32 %tmp_data_V_1, i32 16, i32 23)
  %phitmp2_2_cast = zext i8 %tmp_4_2_cast to i9
  %diff_4 = sub i9 %phitmp2_2_cast, %phitmp1_2_cast
  %tmp_9 = call i1 @_ssdm_op_BitSelect.i1.i9.i32(i9 %diff_4, i32 8)
  %diff_1_2 = sub i9 0, %diff_4
  %diff_2_2 = select i1 %tmp_9, i9 %diff_1_2, i9 %diff_4
  %diff_2_2_cast = sext i9 %diff_2_2 to i10
  %tmp_1 = call i8 @_ssdm_op_PartSelect.i8.i32.i32.i32(i32 %tmp_data_V, i32 24, i32 31)
  %tmp_8_3_cast = zext i8 %tmp_1 to i9
  %tmp_5 = call i8 @_ssdm_op_PartSelect.i8.i32.i32.i32(i32 %tmp_data_V_1, i32 24, i32 31)
  %tmp_4_3_cast = zext i8 %tmp_5 to i9
  %diff_3 = sub i9 %tmp_4_3_cast, %tmp_8_3_cast
  %tmp_10 = call i1 @_ssdm_op_BitSelect.i1.i9.i32(i9 %diff_3, i32 8)
  %diff_1_3 = sub i9 0, %diff_3
  %diff_2_3 = select i1 %tmp_10, i9 %diff_1_3, i9 %diff_3
  %diff_2_3_cast = sext i9 %diff_2_3 to i10
  %tmp3 = add i10 %diff_2_2_cast, %diff_2_1_cast
  %tmp3_cast = sext i10 %tmp3 to i32
  %tmp5 = add i10 %diff_2_3_cast, %diff_2_cast
  %tmp5_cast = sext i10 %tmp5 to i32
  %tmp4 = add i32 %suma, %tmp5_cast
  %suma_1_3 = add nsw i32 %tmp3_cast, %tmp4
  %sum_V = sext i32 %suma_1_3 to i64
  %sum_V_1 = select i1 %tmp_3, i64 %p_0, i64 %sum_V
  %differentBytes_1 = select i1 %tmp_3, i32 %differentBytes, i32 %differentBytes_3
  %suma_2 = select i1 %tmp_3, i32 %suma, i32 %suma_1_3
  br i1 %tmp_last_V, label %.loopexit, label %1

; <label>:1                                       ; preds = %_ifconv
  %empty_20 = call i32 (...)* @_ssdm_op_SpecRegionEnd([7 x i8]* @p_str7, i32 %tmp_4)
  br label %0

.loopexit:                                        ; preds = %_ifconv, %0
  %lhs_V = phi i64 [ %p_0, %0 ], [ %sum_V_1, %_ifconv ]
  %differentBytes_2 = phi i32 [ %differentBytes, %0 ], [ %differentBytes_1, %_ifconv ]
  %in1Count_1 = phi i23 [ %in1Count, %0 ], [ %in1Count_3, %_ifconv ]
  %in1Count_1_cast = zext i23 %in1Count_1 to i32
  call void @_ssdm_op_Write.s_axilite.i32P(i32* %agg_result_a, i32 %differentBytes_2)
  call void @_ssdm_op_Write.s_axilite.i32P(i32* %agg_result_b, i32 %in1Count_1_cast)
  call void @_ssdm_op_Write.s_axilite.i32P(i32* %agg_result_c, i32 %in1Count_1_cast)
  %phitmp = call i32 @_ssdm_op_PartSelect.i32.i64.i32.i32(i64 %lhs_V, i32 32, i32 63)
  call void @_ssdm_op_Write.s_axilite.i32P(i32* %agg_result_d, i32 %phitmp)
  %tmp_11 = trunc i64 %lhs_V to i32
  call void @_ssdm_op_Write.s_axilite.i32P(i32* %agg_result_e, i32 %tmp_11)
  call void @_ssdm_op_Write.s_axilite.i32P(i32* %agg_result_f, i32 1936)
  ret void
}

!opencl.kernels = !{!0, !7, !13, !13, !19, !25, !19, !19, !19, !28, !28, !19, !30, !32, !34, !34, !19, !36, !19, !39, !36, !19, !41, !43, !19, !19, !19, !45, !45, !28, !28, !46, !19, !19, !47, !49, !50, !53, !56, !58, !60, !62, !64, !66, !68, !68, !19, !19, !19, !19, !19, !19, !19, !19, !19, !19, !19, !19, !19, !19, !19, !19, !19, !19, !19, !19, !19, !19, !19, !19}
!hls.encrypted.func = !{}
!llvm.map.gv = !{!70}

!0 = metadata !{null, metadata !1, metadata !2, metadata !3, metadata !4, metadata !5, metadata !6}
!1 = metadata !{metadata !"kernel_arg_addr_space", i32 0, i32 0, i32 0}
!2 = metadata !{metadata !"kernel_arg_access_qual", metadata !"none", metadata !"none", metadata !"none"}
!3 = metadata !{metadata !"kernel_arg_type", metadata !"hls::stream<AXI_VALUE> &", metadata !"hls::stream<AXI_VALUE> &", metadata !"int"}
!4 = metadata !{metadata !"kernel_arg_type_qual", metadata !"", metadata !"", metadata !""}
!5 = metadata !{metadata !"kernel_arg_name", metadata !"INPUT_STREAM", metadata !"LAST_STREAM", metadata !"searched"}
!6 = metadata !{metadata !"reqd_work_group_size", i32 1, i32 1, i32 1}
!7 = metadata !{null, metadata !8, metadata !9, metadata !10, metadata !11, metadata !12, metadata !6}
!8 = metadata !{metadata !"kernel_arg_addr_space", i32 0, i32 0}
!9 = metadata !{metadata !"kernel_arg_access_qual", metadata !"none", metadata !"none"}
!10 = metadata !{metadata !"kernel_arg_type", metadata !"const ap_int_base<64, false> &", metadata !"uint"}
!11 = metadata !{metadata !"kernel_arg_type_qual", metadata !"", metadata !""}
!12 = metadata !{metadata !"kernel_arg_name", metadata !"op", metadata !"i_op"}
!13 = metadata !{null, metadata !14, metadata !15, metadata !16, metadata !17, metadata !18, metadata !6}
!14 = metadata !{metadata !"kernel_arg_addr_space", i32 0}
!15 = metadata !{metadata !"kernel_arg_access_qual", metadata !"none"}
!16 = metadata !{metadata !"kernel_arg_type", metadata !"uint"}
!17 = metadata !{metadata !"kernel_arg_type_qual", metadata !""}
!18 = metadata !{metadata !"kernel_arg_name", metadata !"op"}
!19 = metadata !{null, metadata !20, metadata !21, metadata !22, metadata !23, metadata !24, metadata !6}
!20 = metadata !{metadata !"kernel_arg_addr_space"}
!21 = metadata !{metadata !"kernel_arg_access_qual"}
!22 = metadata !{metadata !"kernel_arg_type"}
!23 = metadata !{metadata !"kernel_arg_type_qual"}
!24 = metadata !{metadata !"kernel_arg_name"}
!25 = metadata !{null, metadata !8, metadata !9, metadata !26, metadata !11, metadata !27, metadata !6}
!26 = metadata !{metadata !"kernel_arg_type", metadata !"const ap_int_base<64, false> &", metadata !"const ap_int_base<32, false> &"}
!27 = metadata !{metadata !"kernel_arg_name", metadata !"op", metadata !"op2"}
!28 = metadata !{null, metadata !14, metadata !15, metadata !29, metadata !17, metadata !18, metadata !6}
!29 = metadata !{metadata !"kernel_arg_type", metadata !"const ap_int_base<32, false> &"}
!30 = metadata !{null, metadata !8, metadata !9, metadata !31, metadata !11, metadata !27, metadata !6}
!31 = metadata !{metadata !"kernel_arg_type", metadata !"const ap_int_base<64, false> &", metadata !"int"}
!32 = metadata !{null, metadata !8, metadata !9, metadata !33, metadata !11, metadata !27, metadata !6}
!33 = metadata !{metadata !"kernel_arg_type", metadata !"const ap_int_base<1, false> &", metadata !"int"}
!34 = metadata !{null, metadata !14, metadata !15, metadata !35, metadata !17, metadata !18, metadata !6}
!35 = metadata !{metadata !"kernel_arg_type", metadata !"int"}
!36 = metadata !{null, metadata !14, metadata !15, metadata !37, metadata !17, metadata !38, metadata !6}
!37 = metadata !{metadata !"kernel_arg_type", metadata !"const ap_int_base<32, true> &"}
!38 = metadata !{metadata !"kernel_arg_name", metadata !"op2"}
!39 = metadata !{null, metadata !8, metadata !9, metadata !40, metadata !11, metadata !27, metadata !6}
!40 = metadata !{metadata !"kernel_arg_type", metadata !"ap_int_base<64, false> &", metadata !"int"}
!41 = metadata !{null, metadata !8, metadata !9, metadata !42, metadata !11, metadata !12, metadata !6}
!42 = metadata !{metadata !"kernel_arg_type", metadata !"const ap_int_base<32, false> &", metadata !"int"}
!43 = metadata !{null, metadata !8, metadata !9, metadata !44, metadata !11, metadata !27, metadata !6}
!44 = metadata !{metadata !"kernel_arg_type", metadata !"const ap_int_base<32, false> &", metadata !"const ap_int_base<32, true> &"}
!45 = metadata !{null, metadata !14, metadata !15, metadata !37, metadata !17, metadata !18, metadata !6}
!46 = metadata !{null, metadata !8, metadata !9, metadata !42, metadata !11, metadata !27, metadata !6}
!47 = metadata !{null, metadata !14, metadata !15, metadata !48, metadata !17, metadata !38, metadata !6}
!48 = metadata !{metadata !"kernel_arg_type", metadata !"const ap_uint<64> &"}
!49 = metadata !{null, metadata !14, metadata !15, metadata !29, metadata !17, metadata !38, metadata !6}
!50 = metadata !{null, metadata !14, metadata !15, metadata !51, metadata !17, metadata !52, metadata !6}
!51 = metadata !{metadata !"kernel_arg_type", metadata !"struct ap_axiu<32, 2, 5, 6> &"}
!52 = metadata !{metadata !"kernel_arg_name", metadata !"dout"}
!53 = metadata !{null, metadata !14, metadata !15, metadata !54, metadata !17, metadata !55, metadata !6}
!54 = metadata !{metadata !"kernel_arg_type", metadata !"const struct ap_axiu<32, 2, 5, 6> &"}
!55 = metadata !{metadata !"kernel_arg_name", metadata !""}
!56 = metadata !{null, metadata !14, metadata !15, metadata !57, metadata !17, metadata !38, metadata !6}
!57 = metadata !{metadata !"kernel_arg_type", metadata !"const ap_uint<6> &"}
!58 = metadata !{null, metadata !14, metadata !15, metadata !59, metadata !17, metadata !38, metadata !6}
!59 = metadata !{metadata !"kernel_arg_type", metadata !"const ap_uint<5> &"}
!60 = metadata !{null, metadata !14, metadata !15, metadata !61, metadata !17, metadata !38, metadata !6}
!61 = metadata !{metadata !"kernel_arg_type", metadata !"const ap_uint<1> &"}
!62 = metadata !{null, metadata !14, metadata !15, metadata !63, metadata !17, metadata !38, metadata !6}
!63 = metadata !{metadata !"kernel_arg_type", metadata !"const ap_uint<2> &"}
!64 = metadata !{null, metadata !14, metadata !15, metadata !65, metadata !17, metadata !38, metadata !6}
!65 = metadata !{metadata !"kernel_arg_type", metadata !"const ap_uint<4> &"}
!66 = metadata !{null, metadata !14, metadata !15, metadata !67, metadata !17, metadata !38, metadata !6}
!67 = metadata !{metadata !"kernel_arg_type", metadata !"const ap_uint<32> &"}
!68 = metadata !{null, metadata !14, metadata !15, metadata !35, metadata !17, metadata !69, metadata !6}
!69 = metadata !{metadata !"kernel_arg_name", metadata !"val"}
!70 = metadata !{metadata !71, [0 x i32]* @llvm_global_ctors_0}
!71 = metadata !{metadata !72}
!72 = metadata !{i32 0, i32 31, metadata !73}
!73 = metadata !{metadata !74}
!74 = metadata !{metadata !"llvm.global_ctors.0", metadata !75, metadata !"", i32 0, i32 31}
!75 = metadata !{metadata !76}
!76 = metadata !{i32 0, i32 0, i32 1}
!77 = metadata !{metadata !78}
!78 = metadata !{i32 0, i32 31, metadata !79}
!79 = metadata !{metadata !80}
!80 = metadata !{metadata !"agg.result.a", metadata !75, metadata !"int", i32 0, i32 31}
!81 = metadata !{metadata !82}
!82 = metadata !{i32 0, i32 31, metadata !83}
!83 = metadata !{metadata !84}
!84 = metadata !{metadata !"agg.result.b", metadata !75, metadata !"int", i32 0, i32 31}
!85 = metadata !{metadata !86}
!86 = metadata !{i32 0, i32 31, metadata !87}
!87 = metadata !{metadata !88}
!88 = metadata !{metadata !"agg.result.c", metadata !75, metadata !"int", i32 0, i32 31}
!89 = metadata !{metadata !90}
!90 = metadata !{i32 0, i32 31, metadata !91}
!91 = metadata !{metadata !92}
!92 = metadata !{metadata !"agg.result.d", metadata !75, metadata !"int", i32 0, i32 31}
!93 = metadata !{metadata !94}
!94 = metadata !{i32 0, i32 31, metadata !95}
!95 = metadata !{metadata !96}
!96 = metadata !{metadata !"agg.result.e", metadata !75, metadata !"int", i32 0, i32 31}
!97 = metadata !{metadata !98}
!98 = metadata !{i32 0, i32 31, metadata !99}
!99 = metadata !{metadata !100}
!100 = metadata !{metadata !"agg.result.f", metadata !75, metadata !"int", i32 0, i32 31}
!101 = metadata !{metadata !102}
!102 = metadata !{i32 0, i32 31, metadata !103}
!103 = metadata !{metadata !104}
!104 = metadata !{metadata !"INPUT_STREAM.V.data.V", metadata !75, metadata !"uint32", i32 0, i32 31}
!105 = metadata !{metadata !106}
!106 = metadata !{i32 0, i32 3, metadata !107}
!107 = metadata !{metadata !108}
!108 = metadata !{metadata !"INPUT_STREAM.V.keep.V", metadata !75, metadata !"uint4", i32 0, i32 3}
!109 = metadata !{metadata !110}
!110 = metadata !{i32 0, i32 3, metadata !111}
!111 = metadata !{metadata !112}
!112 = metadata !{metadata !"INPUT_STREAM.V.strb.V", metadata !75, metadata !"uint4", i32 0, i32 3}
!113 = metadata !{metadata !114}
!114 = metadata !{i32 0, i32 1, metadata !115}
!115 = metadata !{metadata !116}
!116 = metadata !{metadata !"INPUT_STREAM.V.user.V", metadata !75, metadata !"uint2", i32 0, i32 1}
!117 = metadata !{metadata !118}
!118 = metadata !{i32 0, i32 0, metadata !119}
!119 = metadata !{metadata !120}
!120 = metadata !{metadata !"INPUT_STREAM.V.last.V", metadata !75, metadata !"uint1", i32 0, i32 0}
!121 = metadata !{metadata !122}
!122 = metadata !{i32 0, i32 4, metadata !123}
!123 = metadata !{metadata !124}
!124 = metadata !{metadata !"INPUT_STREAM.V.id.V", metadata !75, metadata !"uint5", i32 0, i32 4}
!125 = metadata !{metadata !126}
!126 = metadata !{i32 0, i32 5, metadata !127}
!127 = metadata !{metadata !128}
!128 = metadata !{metadata !"INPUT_STREAM.V.dest.V", metadata !75, metadata !"uint6", i32 0, i32 5}
!129 = metadata !{metadata !130}
!130 = metadata !{i32 0, i32 31, metadata !131}
!131 = metadata !{metadata !132}
!132 = metadata !{metadata !"LAST_STREAM.V.data.V", metadata !75, metadata !"uint32", i32 0, i32 31}
!133 = metadata !{metadata !134}
!134 = metadata !{i32 0, i32 3, metadata !135}
!135 = metadata !{metadata !136}
!136 = metadata !{metadata !"LAST_STREAM.V.keep.V", metadata !75, metadata !"uint4", i32 0, i32 3}
!137 = metadata !{metadata !138}
!138 = metadata !{i32 0, i32 3, metadata !139}
!139 = metadata !{metadata !140}
!140 = metadata !{metadata !"LAST_STREAM.V.strb.V", metadata !75, metadata !"uint4", i32 0, i32 3}
!141 = metadata !{metadata !142}
!142 = metadata !{i32 0, i32 1, metadata !143}
!143 = metadata !{metadata !144}
!144 = metadata !{metadata !"LAST_STREAM.V.user.V", metadata !75, metadata !"uint2", i32 0, i32 1}
!145 = metadata !{metadata !146}
!146 = metadata !{i32 0, i32 0, metadata !147}
!147 = metadata !{metadata !148}
!148 = metadata !{metadata !"LAST_STREAM.V.last.V", metadata !75, metadata !"uint1", i32 0, i32 0}
!149 = metadata !{metadata !150}
!150 = metadata !{i32 0, i32 4, metadata !151}
!151 = metadata !{metadata !152}
!152 = metadata !{metadata !"LAST_STREAM.V.id.V", metadata !75, metadata !"uint5", i32 0, i32 4}
!153 = metadata !{metadata !154}
!154 = metadata !{i32 0, i32 5, metadata !155}
!155 = metadata !{metadata !156}
!156 = metadata !{metadata !"LAST_STREAM.V.dest.V", metadata !75, metadata !"uint6", i32 0, i32 5}
!157 = metadata !{metadata !158}
!158 = metadata !{i32 0, i32 31, metadata !159}
!159 = metadata !{metadata !160}
!160 = metadata !{metadata !"searched", metadata !161, metadata !"int", i32 0, i32 31}
!161 = metadata !{metadata !162}
!162 = metadata !{i32 0, i32 0, i32 0}
